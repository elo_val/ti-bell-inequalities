import numpy as np
from fractions import Fraction
import itertools
import networkx as nx

try:
    from common_utils import tostring, decompose_number, frac_array
except:
    from .common_utils import tostring, decompose_number, frac_array

# ========== Construct correlator vector ========== #
def one_body_corr_vec(strat,m,d):
    """strat (int) is the strategy. """
    if d != 2:
        raise Exception('Implemented only for d=2')
    omega = -1
    fs = decompose_number(number=strat,base=d, length=m)
    out = np.zeros(m,dtype=int)
    for x in range(m):
        out[x] = omega**(fs[x])
    return out

def two_body_corr_vec(strat1,strat2,m,d):
    return np.kron(one_body_corr_vec(strat=strat1,m=m,d=d),one_body_corr_vec(strat=strat2,m=m,d=d))

def symmetric_R_corr_vec(strategy_vector,m,d,R):
    """Returns the symmetric correlator vector 
    with 2 body correlators and interaction range R."""
    if len(strategy_vector) != R+1:
        # The input circuit should have length R+1.
        raise Exception('The number of strategies must be equal to R+1')
    s = strategy_vector
    out = []

    # One body correlators
    tmp = np.sum([one_body_corr_vec(strat=s[i%len(s)],m=m,d=d) for i in range(R+1)],axis=0)
    tmp = np.vectorize(lambda x:Fraction(x,R+1),otypes=[Fraction])(tmp) # convert to numpy array of Fraction
    out = np.concatenate((out,tmp),dtype=Fraction)

    # Two body correlators
    for r in range(1,R+1):
        tmp = np.sum([two_body_corr_vec(strat1=s[i%len(s)],strat2=s[(i+r)%len(s)],m=m,d=d) for i in range(R+1-r)],axis=0)
        tmp = np.vectorize(lambda x:Fraction(x,R+1-r),otypes=[Fraction])(tmp)
        out = np.concatenate((out,tmp),dtype=Fraction)
    return out

def corr_vec(strategy_vector,m,d,R):
    """Returns the correlator vector associated to the strategy-vector.
    The correlator vector is normalized by the length of the strategy-vector"""
    s = (R+1)*tuple(strategy_vector)
    out = np.zeros(m+R*m**2,dtype=Fraction)
    for i in range(len(strategy_vector)):
        out += symmetric_R_corr_vec(strategy_vector=s[i:i+1+R],m=m,d=d,R=R)
    return out/len(strategy_vector) # for the normalized divide by len(strategy_vector)

def TI_R_corr_vec(scenario,R,strategy_vector,):
    """Alias for the corr_vec method with sanity checks.
    Returns the correlator vector associated to the 
    translation invariant up to 2 body and interaction range R projection."""
    N,m,d = scenario
    if len(strategy_vector) != N:
        raise Exception('The number of strategies must be equal to N')
    return corr_vec(strategy_vector=strategy_vector,m=m,d=d,R=R)

# ===== Phi map ===== # 

def get_corr_vec_associated_to_edge(graph,edge):
    """Gets the correlator vector associated to the edge.

    Args:
        graph (networkx.DiGraph): graph with attributes 'interaction range' and 'number of strategies'.
                                  The edges of the graph must have the attribute 'strat' for the edge (i,j)

        edge (tuple of int): pair of integer that idetifies the ege.

    Returns:
        1D numpy array of Fraction: the correlator vector associated to the edge. The correlator vector is also updated in the graph.
    """
    edge_index = tuple(edge)
    edge = graph.edges[edge_index]
    corr_vec = edge.get('correlator vector')
    if corr_vec is not None:
        return frac_array(corr_vec)
    else:
        d = 2
        R = graph.graph['interaction range'] # The input graph must be a De Bruijn graph with the attribute "interaction range"
        Nstrat = graph.graph['number of strategies'] # The input graph must be a De Bruijn graph with the attribute "number of strategies"
        m = int(np.log(Nstrat) / np.log(d)) # because Nstrat = d^m (using the logarithm base change rule)
        corr_vec = symmetric_R_corr_vec(strategy_vector=edge['strat'],m=m,R=R,d=d)
        nx.set_edge_attributes(graph, {edge_index: {"correlator vector": tuple(corr_vec)}})
        return corr_vec

def Phi_map(graph,w):
    """Implements equation 36 in the paper. 
    Works only for debruijn graph."""
    tmp = []
    for i,j in itertools.product(*tuple(range(n) for n in np.shape(w))):
        if w[i,j] != 0:
            corr_vec = get_corr_vec_associated_to_edge(graph=graph,edge=(i,j))
            tmp.append(w[i,j]*corr_vec)
    return np.sum(tmp,axis=0)

# ===== Examples ===== #

def example_1():
    print("The four one-body correlator vectors in the case m=d=2")
    m = 2 # number of inputs
    d = 2 # number of outcomes
    for s in range(d**m): # there are 4 strategies in the case m=d=2
        print(f'\t<A>_{s} = ',one_body_corr_vec(strat=s,m=m,d=d))


def example_2():
    print("This example corresponds to the figure 4 in the paper")
    N = 5 # number of parties
    m = 2 # number of inputs
    d = 2 # number of outcomes
    R = 1 # interaction range
    scenario = (N,m,d)

    s = (0,0,1,3,1) # strategy vector of length N

    # Sum over the edges
    edges = [(s[i],s[(i+1)%N]) for i in range(N)]
    tmp2 = np.sum([symmetric_R_corr_vec(strategy_vector=s,m=m,d=d,R=R) for s in edges],axis=0)/N
    print('\tSum of the edges:',tostring(tmp2))

    # TI-R projection
    tmp1 = TI_R_corr_vec(scenario=scenario,R=R,strategy_vector=s)
    print('\tTINN projection of the corresponding strategy:',tostring(tmp1))

def example_3():
    try:
        import graph_utils
    except:
        from . import graph_utils

    print("This example shows that the TI-R projection and the Phi-map are equivalent")
    N = 5 # number of parties
    m = 2 # number of inputs
    d = 2 # number of outcomes
    R = 1 # interaction range
    scenario = (N,m,d)

    G = graph_utils.debruijn_graph(Nstrat=d**m,R=R)

    s = (0,0,1,3,1) # strategy vector of length N

    # Phi-map of the associated weight matrix
    w = np.zeros((d**m,d**m),dtype=object)
    for i in range(len(s)):
        w[s[i],s[(i+1)%N]] += Fraction(1,N)
    tmp = Phi_map(graph=G,w=w)
    print(f'\tPhi-map: {tostring(tmp)}')

    # TI-R projection
    tmp1 = TI_R_corr_vec(scenario=scenario,R=R,strategy_vector=s)
    print('\tTINN projection of the corresponding strategy:',tostring(tmp1))

    print(G.edges[0,0]['correlator vector'])

if __name__ == "__main__":
    example_1()
    example_2()
    example_3()

