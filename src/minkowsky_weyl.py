import numpy as np
import scipy
from tqdm import tqdm
import subprocess
import datetime
import os

try:
    from common_utils import tostring, frac_array
except:
    from .common_utils import tostring, frac_array

# === Compute rays and vertices === # 

def conical_comb(vector, vectors):
    if len(vectors) == 0:
        # If there are no other vectors, the vector is necessarly a ray.
        return None
    
    n,_ = np.shape(vectors)

    c = np.zeros(n)
    A_eq = np.transpose(vectors)
    b_eq = vector
    bounds = (0,None)
    sol = scipy.optimize.linprog(c=c,A_eq=A_eq,b_eq=b_eq,bounds=bounds)

    if sol.success:
        return np.asarray(sol.x)
    elif sol.status == 2: # problem is infeasible -> not a conical combination
        return None
    else:
        raise Exception('An error occured: scipy-solution-status =',sol.status)
    
def polyhedral_comb(point,points=None,rays=None):
    if points is None or len(points) == 0:
        # If there are no other points, the point is necessarly a vertex -> wrong! what if the rays span the full space?
        return None
        # If there are no other points, then we have a polyhedral cone. The point is a vertex, if it is not a convex combination of the translated cone.
        # Idea: The translation is done in the directionof the rays
    if rays is None or len(rays) == 0:
        n,dim = np.shape(points)
        m = 0
        rays = np.empty((m,dim)) # set an empty array with correct dimension
    else:
        m,dim = np.shape(rays)
        n,dim_ = np.shape(points)
        if dim !=dim_:
            raise Exception(f"The dimensions of the vectors (dim={dim}) and the points (dim={dim_}) don't match")

    c = np.zeros(n+m)
    all_points_and_rays = np.concatenate((points,rays))
    convex_comb_constraint = np.concatenate((np.ones((1,n)),np.zeros((1,m))),axis=1)
    A_eq = np.concatenate((np.transpose(all_points_and_rays),convex_comb_constraint),axis=0)
    b_eq = np.concatenate((point, [1]),axis=0)
    bounds = (0,None)
    sol = scipy.optimize.linprog(c=c,A_eq=A_eq,b_eq=b_eq,bounds=bounds)

    if sol.success:
        return np.asarray(sol.x)
    elif sol.status == 2: # problem is infeasible -> not a convex combination
        return None
    else:
        raise Exception('An error occured: scipy-solution-status =',sol.status)
    
def rays_and_vertices_polyhedron(points=None,vectors=None,verbose=False,return_indices=False):
    if points is None and vectors is None:
        raise Exception('Specify either points of vectors, or both.')
    elif points is None:
        vectors = np.asarray(vectors)
        m,dim = np.shape(vectors)
        n = 1
        points = np.zeros((1,dim)) # set the zero vector as unique vertex
    elif vectors is None:
        points = np.asarray(points)
        n,dim = np.shape(points)
        m = 0
        vectors = np.empty((0,dim)) # set an empty array with correct dimension
    else:
        vectors = np.asarray(vectors)
        points = np.asarray(points)
        m,dim = np.shape(vectors)
        n,dim_ = np.shape(points)
        if dim !=dim_:
            raise Exception(f"The dimensions of the vectors (dim={dim}) and the points (dim={dim_}) don't match")
    
    # Compute the rays
    rays = []
    interior_vectors = []
    pbar = tqdm(range(m),desc=f"Computing rays, processed {0}/{m} vectors, rays={len(rays)}, interior vectors={len(interior_vectors)}",disable=not verbose)
    for i in pbar:
        vector = vectors[i]
        vectors_subset = np.delete(vectors, interior_vectors+[i], axis=0) # subset of vectors containing the rays
        tmp2 = conical_comb(vector=vector,vectors=vectors_subset)
        if tmp2 is not None:
            interior_vectors.append(i)
        else:
            rays.append(i)
        pbar.set_description(f"Computing rays, processed {i+1}/{m} vectors, rays={len(rays)}, interior vectors={len(interior_vectors)}")
    rays_indices = rays
    rays = vectors[rays_indices,:]

    # Compute the vertices
    interior_points = []
    vertices = []
    pbar = tqdm(range(n),desc=f"Computing vertices, processed {0}/{n} points, vertices={len(vertices)}, interior points={len(interior_points)}",disable=not verbose)
    for i in pbar:
        point = points[i]
        points_subset = np.delete(points, interior_points+[i], axis=0) # subset of points containing the vertices
        tmp = polyhedral_comb(point=point,points=points_subset,rays=rays)
        if tmp is not None:
            interior_points.append(i)
        else:
            vertices.append(i)
        pbar.set_description(f"Computing vertices, processed {i+1}/{n} points, vertices={len(vertices)}, interior points={len(interior_points)}")
    vertices_indices = vertices
    vertices = points[vertices_indices,:]

    if return_indices:
        return rays_indices,vertices_indices
    else:
        return rays,vertices

# ==== Compute inequalities === #

def equalities_and_inequalities_polyhedron(points, method='ad',keep_files=False, verbose=False):
    """Compute the equalities and inequalities defining the polyhedron with PANDA.
    The argument method being either "adjacency-decomposition" ("ad", default) or "double-description" ("dd").
    The format of the (in)equality is (alpha,beta) -> alpha*x >= beta
    """
    file_input = 'panda_input.txt' # temporary file
    file_output = 'panda_outcome.txt' # temporaty file

    #points,scale_factor = rescale_to_int(points)

    # Prepare the input file with the vertices for PANDA
    text= 'Vertices:\n'
    for point in points:
        for element in point:
            text += str(element) + ' '
        text += '\n'
    with open(file_input, 'w') as file:
        file.write(text)

    t0 = datetime.datetime.now()
    if verbose:
        print(f'{t0}: Computing the H-representation of the convex hull of {np.shape(points)[0]} points of dimension {np.shape(points)[1]} with PANDA. Might take some time, please be patient.')

    # Run PANDA: give the input file and produces an output file
    bashCommand = f"panda --method={method} {file_input} > {file_output}"
    subprocess.run(bashCommand,shell=True, stdout=subprocess.DEVNULL, stderr=subprocess.DEVNULL)

    t1 = datetime.datetime.now()
    if verbose:
        print(f'{t1}: Finished. Ellapsed time {t1-t0}')

    # Parse PANDA output file
    eq = []
    ineq = []
    with open(file_output, 'r') as file:
        lines = file.readlines()
        section = None
        for line in lines:
            line = line.strip()
            if line == "Equations:":
                section = "equations"
            elif line == "Inequalities:":
                section = "inequalities"
            elif line and section:
                # panda format: [a1,a2,...,b] <-> a*x + b = 0 or a*x + b <= 0
                values = list(map(int, line.split()))  
                # convention here: (alpha;beta) <-> alpha*x >= beta
                alpha = [-a for a in values[:-1]] # alpha = -a
                beta = values[-1] #/scale_factor # beta = b
                if section == "equations":
                    eq.append(alpha + [beta])
                elif section == "inequalities":
                    ineq.append(alpha + [beta])

    if not keep_files:
        # Delete temporary files
        os.remove(file_input)
        os.remove(file_output)
                    
    return frac_array(eq), frac_array(ineq)

def main():
    # import itertools

    # # Test 1
    # points = frac_array([list(v) for v in itertools.product(('-1/2','1/2'),repeat=3)]) # define a hypercube
    # vectors = frac_array([[1,0,0]])
    # rays,vertices = rays_and_vertices_polyhedron(points=points,vectors=vectors,verbose=True)
    # print(f'rays = {tostring(rays)}')
    # print(f'vertices = {tostring(vertices)}')

    # print('===')
    # # Test 2
    # points = frac_array([[0,0],['1/2',0],[0,'1/2'],['1/2','1/2']])
    # vectors = frac_array([[0,1],[1,0]])
    # rays,vertices = rays_and_vertices_polyhedron(points=points,vectors=vectors,verbose=True)
    # print(f'rays = {tostring(rays)}')
    # print(f'vertices = {tostring(vertices)}')

    # print('===')

    # # Test 3: no vectors
    # Npoints = 50
    # dim = 15
    # points = frac_array(np.random.rand(Npoints,dim).tolist() + [list(v) for v in itertools.product([0,1],repeat=dim)])
    # rays,vertices = rays_and_vertices_polyhedron(points=points,verbose=True)
    # print(f'rays = {tostring(rays)}')
    # print(f'vertices = {tostring(vertices)}')

    # # Test 4: no points
    # Nvec = 20
    # dim = 3
    # vectors = frac_array(np.random.rand(Nvec,dim).tolist() + [list(v) for v in itertools.product([0,1],repeat=dim)])
    # rays,vertices = rays_and_vertices_polyhedron(vectors=vectors,verbose=True)
    # print(f'rays = {tostring(rays)}')
    # print(f'vertices = {tostring(vertices)}')

    # # # Test 4: high dimensional case
    # Npoints = 3000
    # Nvec = 3000
    # dim = 10
    # vectors = frac_array(np.concatenate((np.random.rand(Npoints,dim-2),np.zeros((Npoints,2))),axis=1).tolist())
    # points = frac_array(np.random.rand(Npoints,dim).tolist() + [list(v) for v in itertools.product([0,1],repeat=dim)])
    # rays,vertices = rays_and_vertices_polyhedron(points=points, vectors=vectors,verbose=True)
    # print(f'Number of rays = {len(rays)}')
    # print(f'Number of vertices = {len(vertices)}')

    # Test 5: No vertex at all
    vectors = frac_array([[1,0],[-1,0]])
    points = frac_array([[1,1]])
    rays,vertices = rays_and_vertices_polyhedron(points=points,vectors=vectors,verbose=True)
    print(f'Number of rays = {len(rays)}')
    print(f'Number of vertices = {len(vertices)}')


if __name__ == "__main__":
    main()