import numpy as np
import networkx as nx
import itertools

try:
    import graph_utils
    import taminplus
except:
    from . import graph_utils
    from . import taminplus

def kleeneplus(F,k):
    return taminplus.A_plus_n(A=F,k=k)

def compute_eigval(F):
    """Alias for Karps algorithm"""
    n,_ = np.shape(F)
    out = np.inf
    for node in range(n):
        out = np.minimum(out,taminplus.karps_algorithm(F,node))
    return out

def compute_eigvecs(F,eigval=None):
    if eigval is None:
        eigval = compute_eigval(F)
    eigvecs = set(tuple(v) for v in taminplus.trop_eigenvector(A=F,lam=eigval))
    return eigval,eigvecs

def get_strongly_connected_submatrices(F):
    """compute a set of matrices such that each of them represent a strongly connected subgraph of F."""
    m,_ = np.shape(F)
    G = graph_utils.matrix_to_graph(M=F,neutral_element=np.inf)
    out = []
    for strongly_connected_nodes in nx.strongly_connected_components(G):
        tmp = np.copy(F)
        for node in range(m):
            if node not in strongly_connected_nodes:
                tmp[node,:] = np.full(m,np.inf)
                tmp[:,node] = np.full(m,np.inf)
        out.append(tmp)
    return out

def compute_critical_graph(F,eigval=None,eigvecs=None):
    n,m = np.shape(F)

    if eigvecs is None:
        eigval,eigvecs = compute_eigvecs(F,eigval=eigval)

    F_crit = np.full(np.shape(F),np.inf,dtype=object) # the tropical null-matrix
    for v in eigvecs:
        # For each eigen vector, we construc the sub-graph only with the edges (l,k) satisfying: F[k,l] + v[l] - v[k] = lambda(F)
        F_v = np.full(np.shape(F),np.inf,dtype=object) # the tropical null-matrix
        for k,l in itertools.product(range(n),range(m)):
            if F[k,l] + v[l] - v[k] == eigval:
                F_v[k,l] = F[k,l]
        # Uninon of strongly connected components. For the matrices it corresponds to the tropical addition
        for tmp in get_strongly_connected_submatrices(F_v):
            F_crit = np.minimum(F_crit,tmp) # tropical addition
    return F_crit

def cyclicity(F,F_crit=None):
    if F_crit is None:
        F_crit = compute_critical_graph(F)
    lcm = 1
    for sub_F in get_strongly_connected_submatrices(F_crit):
        sub_G = graph_utils.matrix_to_graph(M=sub_F,neutral_element=np.inf)
        gcd = np.gcd.reduce([len(c) for c in nx.simple_cycles(sub_G)])
        lcm = np.lcm(lcm,gcd)
    return lcm

def stabilization_numbers(F,sigma=None):
    if sigma is None:
        sigma = cyclicity(F)
    N0 = taminplus.tropicalrenorm_n(tensor_F=F,k=sigma)
    return sigma,N0

def main():
    F = np.array([[2,4,-4,-2],[0,2,-2,0],[0,-2,2,0],[-2,-4,4,2]])
    print(f'Tropical quantities of the matrix\n{F}')
    eigval = compute_eigval(F)
    print(f'Eigenvalue: {eigval}')
    eigval,eigvecs = compute_eigvecs(F,eigval=eigval)
    print(f'Eigenvectors: {eigvecs}')
    F_crit = compute_critical_graph(F,eigval=eigval,eigvecs=eigvecs)
    print(f'Weight matrix of the critical graph:\n{F_crit}')
    G_crit = graph_utils.matrix_to_graph(M=F,neutral_element=np.inf,label='weight')
    graph_utils.draw_graph(G_crit,node_label=True,edge_label='weight')
    sigma = cyclicity(F)
    print(f'The cyclicity: sigma = {sigma}')
    sigma,N0 = stabilization_numbers(F,sigma=sigma)
    print(f'The stabilization numbers are sigma = {sigma} and N0 = {N0}')

if __name__ == "__main__":
    main()