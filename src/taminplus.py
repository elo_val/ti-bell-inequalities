import numpy as np


def max_plus_product(A, B):
    """Compute the tropical matrix multiplication of the matrices A and B. 

    Args:
        A (np.ndarray): Input Matrix A
        B (np.ndarray): Input Matrix B

    Returns:
        np.ndarray: Tropical matrix product A@B
    """
    n, k = A.shape
    if B.ndim == 1:
        k = B.shape[0]
        B = B.reshape((k, 1))
    k, m = B.shape
    C = np.zeros((n, m))
    
    for i in range(n):
        for j in range(m):
            maximum = A[i, 0] + B[0, j]
            for l in range(0, k):
                maximum= min(A[i, l] + B[l, j], maximum)
            C[i, j] = maximum
    
    return C


def max_plus_add(A, B):
    """Compute the tropical matrix addition of the matrices A and B. 

    Args:
        A (np.ndarray): Input Matrix A
        B (np.ndarray): Input Matrix B

    Returns:
        np.ndarray: Tropical matrix plus A+B
    """
    n, k = A.shape
    if isinstance(B, (int, float)):
        C = np.zeros((n, k))
        for i in range(n):
            for j in range(k):
                C[i, j] = max(A[i, j], B)
    else:
        k, m = B.shape
        C =np.zeros((n, m))
        for i in range(n):
            for j in range(m):
                C[i, j] = max(A[i, j], B[i, j])
    return C

def min_plus_product(A, B):
    """Compute the tropical matrix multiplication of the matrices A and B. 

    Args:
        A (np.ndarray): Input Matrix A.
        B (np.ndarray or float): Input Matrix B (can be a vector or a constant).

    Returns:
        np.ndarray: Tropical matrix product A @ B.
    """
    n, k = A.shape
    if B.ndim == 1:
        k = B.shape[0]
        B = B.reshape((k, 1))
    k, m = B.shape
    C = np.zeros((n, m))
    for i in range(n):
        for j in range(m):
            minimum = A[i, 0] + B[0, j]
            for l in range(0, k):
                minimum = min(A[i, l] + B[l, j], minimum)
            C[i, j] = minimum
    return C

def min_plus_add(A, B):
    """Compute the tropical matrix addition of the matrices A and B. 

    Args:
        A (np.ndarray): Input Matrix A
        B (np.ndarray): Input Matrix B

    Returns:
        np.ndarray: Tropical matrix plus A+B
    """
    n, k = A.shape
    if isinstance(B, (int, float)):
        C = np.zeros((n, k))
        for i in range(n):
            for j in range(k):
                C[i, j] = min(A[i, j], B)
    else:
        k, m = B.shape
        C = np.zeros((n, m))
        for i in range(n):
            for j in range(m):
                C[i, j] = min(A[i, j], B[i, j])
    return C


def matrix_A_power_k(A,k):
    """Comput the exponential function A to the power k

    Args:
        A (np.ndarray): Input matrix A
        k (integer): a positive integer

    Returns:
        np.ndarray: exponential function: A to the power k
    """
    i=1
    A_i=A
    while i<k:
        A_i=min_plus_product(A,A_i)
        i=i+1
    return A_i

#list the tropical matrix power A^{\odot 1}, \cdots, A^{\odot k}
# def list_matrix_A_power_k(A,k):
#     print(A)
#     i=1
#     A_i=A
#     while i< k:
#         A_i=min_plus_product(A,A_i)
#         print(A_i)
#         i=i+1

def A_plus(A):
    """Compute the Kleene plus of A: A_plus= oplus_n A^{odot n}, n is the dimension of A

    Args:
        A (np.ndarray): Input matrix A

    Returns:
        np.ndarray: Kleene plus of A:: A_plus=oplus_n A^{odot n}, n is the dimension of A
    """
    if len(A)!=len(A[0]):
        print("Error: The matrix should be square matrix.")
    else: 
        A_list=[]
        for i in range(1,(len(A)+1)):
            A_i=matrix_A_power_k(A,i)
            A_list.append(A_i)
    j=1
    aplus=A_list[0]
    while j <=(len(A)-1):
        aplus=min_plus_add(A_list[j],aplus)
        j=j+1
    return aplus

def trop_trace(F):
    """compute the tropcial trace (minimum diagonal entry) of a given matrix F

    Args:
        F (np.ndarray): a given square matrix

    Returns:
        float: the minimum diagonal entry of F
    """
    num_rows, num_cols = F.shape
    diagonal_elements = []
    for i in range(min(num_rows, num_cols)):
        diagonal_elements.append(F[i, i])
    min_diagonal_element = min(diagonal_elements)
    return min_diagonal_element

def karps_algorithm(A, j):
    """
    Compute the minimum mean length of cycles in matrix A using Karp's Algorithm.

    Args:
        A (np.ndarray): Input matrix A represented as a NumPy array.
        j (int): Index of the starting node for the algorithm.

    Returns:
        float: The minimum mean length of cycles, which is the tropical eigenvalue of A.
    """
    n=len(A)
    x_0=[]
    catalogue=[]
    for l in range(1,n+1):
        if l==j:
            x_0.append(0)
        else:
            x_0.append(np.inf)
    catalogue.append(np.transpose([x_0]))
    for k in range(1,n+1):
        x_k=min_plus_product(A,catalogue[k-1])
        catalogue.append(x_k)
    candidates=[]
    for i in range(n):
        m_i=[]
        for k in range(n):
            if catalogue[n][i][0]!=np.inf and catalogue[k][i][0]!=np.inf:
                m_i.append((catalogue[n][i][0]-catalogue[k][i][0])/(n-k))
            elif catalogue[n][i][0]==np.inf and catalogue[k][i][0]==np.inf:
                m_i.append(0)
        if m_i:
            candidates.append(max(m_i))
    if candidates:
            eigenval=min(candidates)
            return eigenval


def trop_eigenvector(A, lam):
    """Compute the tropical eigenvectors of matrix A with eigenvalue lambda

    Args:
        A (np.ndarray): Input matrix A
        lam (float): tropical eigenvalue of matrix A

    Returns:
        List[np.ndarray]: tropical eigenvectors of matrix A
    """
    Alambda = []
    tolerance = 1e-6  # the tolerance
    for i in range(len(A)):
        Alambda_i = []
        for j in range(len(A)):
            Alambda_i.append(A[i][j] - lam)
        Alambda.append(Alambda_i)
    Aplus = A_plus(np.array(Alambda))
    eigenspace = []
    #Aplus=np.transpose(Aplus)
    for k in range(len(A)):
        # if np.isclose(Aplus[k][k], 0):
        is_close_to_zero = np.isclose(Aplus[k][k], 0, atol=tolerance)
        if is_close_to_zero:
            a_k = np.transpose([Aplus[:,k]])
            eigenspace.append(a_k)
    unique_eigenspace = []
    for line in eigenspace:
        flattened_line = line.ravel()
        line_tuple = tuple(flattened_line)  
        if line_tuple in unique_eigenspace:
            break
        else:
            unique_eigenspace.append(line_tuple)
    return unique_eigenspace


def A_plus_n(A,k):
    """Compute the Kleene plus of A: A_plus=oplus_k A^{odot k}

    Args:
        A (np.ndarray): Input matrix A
        k (integer): a nonnegative integer

    Returns:
        np.ndarray: Kleene plus of A: A_plus=oplus_k A^{odot k}
    """
    if len(A)!=len(A[0]):
        print("Error: The matrix shoule be square matrix.")
    else: 
        A_list=[]
        min_value = np.min(A)
        Amin = A - min_value
        for i in range(1,k+1):
            A_i=matrix_A_power_k(Amin,i)
            A_list.append(A_i)
    j=1
    aplus=A_list[0]
    while j <=(k-1):
        aplus=min_plus_add(A_list[j],aplus)
        j=j+1
    return aplus

def tropicalrenorm_Aplus(A):
    """Compute minimum n such that F^⊕{n+1} = λ⨁F^⊕(n)

    Args:
        A (np.ndarray): Input matrix A

    Returns:
        integer: minimum n such that F^⊕{n+1} = λ⨁F^⊕(n)
    """
    res = []
    k = len(A)
    i = 1
    A_normalize=A-np.min(A)
    while i < k + 1:
        B= A_plus_n(A_normalize, i + 1)
        #print(A)
        C= A_plus_n(A_normalize, i)
        res = B- C
        if np.all(np.abs(res) <= 0.0001):
            return i + 1
        else:
            i = i + 1

def tropicalrenorm_Atimes(A):
    """Comput minimum n such that F^otimes n = lambda oplus F^{otimes (n-1)}

    Args:
        A (np.ndarray): Input matrix A

    Returns:
        integer: minimum n such that F^{otimes n} = lambda oplus F^{otimes (n-1)}
    """
    res = []
    k = len(A)
    i = 1
    #A_normalize=A-np.min(A)
    while i < k + 1:
        B=matrix_A_power_k(A, i + 1)
        #print(A)
        C= matrix_A_power_k(A, i)
        res = B- C
        if np.min(res)==np.max(res):
            return i+1
        else:
            i = i + 1


def tropicalrenorm_n(tensor_F,k):
    """Compute minimum n+k such that F^{n+k}=lambda^k F^{n}

    Args:
        tensor_F (mp.ndarray): Input matrix tensor_F (encoded from bell ineq )
        k (integer): k is the step gaps, a positive integer

    Returns:
        integer: n is an integer such that F^{n+k}=lambda^k F^{n} when F and k is given
    """
    res=[]
    l=len(tensor_F)
    i=1
    F_la=matrix_A_power_k(tensor_F,1+k)
    F_sm=matrix_A_power_k(tensor_F,1)
    while i<100*l:
        res=np.array(F_la)-np.array(F_sm)
        if np.isclose(np.min(res),np.max(res),rtol=1e-5):
            return i-1
            break
        else:
            i=i+1
            F_la=min_plus_product(F_la,tensor_F)
            F_sm=min_plus_product(F_sm,tensor_F)

#F\otimes v(k)=\lambda \otimes v(k+1) implies min_j F_{kj}+v_j=\lambda+v_k, we want to find j
def optimal_strategy(tensor_F,lam):
    """F otimes v(k)=lambda otimes v(k+1) implies min_j F_{kj}+v_j=lambda+v_k, we want to find j: the optimal strategies of a give tensor F 

    Args:
        tensor_F (np.array:16*16 matrix): the tensor F of a given Bell inequality
        lam (float): the tropical eigenvalue of the tensor F

    Returns:
        a set of optimal strategies {j}: min_j F_{kj}+v_j=lambda+v_k
    """
    #F is the tensor encoded fron an bell inequality
    #lam is the tropcial eigenvalue of tensor F
    k=0
    dim=len(tensor_F)
    eigvec=np.array(trop_eigenvector(tensor_F,lam))
    num_eigenvec=len(eigvec)
    strategylist=[]
    for i in range(num_eigenvec):
        strategy_i=[]
        print('eigenvector:',np.transpose(eigvec[i]))
        strategy=[]
        for k in range(dim):
            for l in range(dim):
                if tensor_F[k][l]+eigvec[i][l]-eigvec[i][k]-lam==0:
                    print('k=',k,'optimal l=',l)
                    strategy.append(l)
        strategy_i.append(strategy)
    strategylist.append(strategy_i)
    return  strategylist       


def main():
    F=np.array([[2,4,-4,-2],[0,2,-2,0],[0,-2,2,0],[-2,-4,4,2]])
    F_2=matrix_A_power_k(F,2) #compute the tropical power F^{\odot 2}
    print("tropical power F^{odot 2}:\n",F_2)
    stabilization_n=tropicalrenorm_n(F,1) 
    print("the stabilization number N_0 is:\n",stabilization_n)
    eig_value=karps_algorithm(F,1)
    print("the tropical eigenvalue of F:\n",eig_value)
    eig_vec=trop_eigenvector(F,eig_value)
    print("tropical eigenvectors of F:\n",eig_vec)

if __name__=='__main__':
    main()