import numpy as np
import networkx as nx
import itertools
import sage.all as sage
from tqdm import tqdm


try:
    import graph_utils
except:
    from . import graph_utils

# ========== weight matrices methods ======== #
# weight-matrices are flatten, i.e. 4x4 matrix -> 16 dimensional vector
# Since we will use these methods a lot, we define here short-hand notations.

GRAPH = None
SIMPLE_CYCLES = None

def set_graph(graph):
    """Set graph for the methods w(c) and W(c)"""
    if list(graph.nodes) != [i for i in range(graph.number_of_nodes())]:
        raise Exception('Nodes must be integer number from 0 to G.number_of_nodes()-1')
    global GRAPH
    GRAPH = graph
    global SIMPLE_CYCLES
    SIMPLE_CYCLES = None # re-initialize the variable

def get_graph():
    return GRAPH

def get_simple_cycles():
    global SIMPLE_CYCLES
    if SIMPLE_CYCLES is None:
        graph = get_graph()
        SIMPLE_CYCLES = graph_utils.get_sorted_simple_cycles(graph=graph)
    return SIMPLE_CYCLES

def W(c):
    G = get_graph()
    return graph_utils.compute_weight_matrix_of_closed_path(graph=G,c=c,normalized=False)

def w(c):
    G = get_graph()
    return graph_utils.compute_weight_matrix_of_closed_path(graph=G,c=c,normalized=True)

# =============== Claims to discard the simplices ==================== #

def is_c0_irreducible(c0,M,return_support=False):
    """Returns True if the matrix M is c0-irreducible.
    If return_support is True, it also returns the c0-support of the matrix M.
    Note: the c0-support of M makes sense only if M is c0-irreducible. Thus,
    If return_support is True and M is not c0-irreducible, the value None is returned
    instead of the support.

    Args:
        c0 (tuple or list of integers): sequence of node. A node is represented by an integer between 0 and n-1, 
                                        where `n = int(np.sqrt(len(M)))` is the total number of nodes.
        M (1D numpy array): represents the flatten nxn matrix, where n is the number of nodes in G.
        return_support (bool, optional): set True to get the support of M as second argument. 
                                         Defaults to False.
                                         If is M is not c0_irreducible, the support is None.

    Returns:
        bool, [set of nodes of G]: True if M is c0-irreducible. Optionally, if return_support is True, 
                                   the support is also returned as a set of nodes.
                                   False if M is not c0-irreducible. If return_support is True, the
                                   value None is returned insted of the support.
    """
    n = int(np.sqrt(len(M)))
    M = np.copy(M).reshape((n,n))
    visited_nodes = set([node for node in c0])
    modified = True
    while modified and not np.all(M==0):
        modified = False
        for i,j in itertools.product(range(n),repeat=2):
            if M[i,j] != 0 and (i in visited_nodes or j in visited_nodes):
                visited_nodes = visited_nodes.union({i,j})
                M[i,j] = 0
                modified = True
                break
    if np.all(M==0):
        if return_support:
            return True, visited_nodes
        else:
            return True
    else:
        if return_support:
            return False, None
        else:
            return False
    
def A_has_no_negative_entries_outside_B(A,B):
    """Return True if the array A has no negative entries outside of B.

    Args:
        A (1D numpy array): an array (usually representing a weigth matrix)
        B (1D numpy array): an array (usually representing a weigth matrix)

    Returns:
        bool: True if the array A has no negative entries outside of B
    """
    for i,val in enumerate(A):
        if val < 0 and B[i] == 0:
            return False
    return True
    
def satisfy_second_claim(c0,S,Ms=None,Ms_is_c0_irred=None,c0_support_of_Ms=None):
    """Return True if the second claim in the appendix ``Polytopes associated to graph 
    -> computing the vertices of p_N(G) -> making the computation more efficient`` is satisfied.

    Args:
        c0 (tuple or list of integers): sequence of node. A node is represented by an integer between 0 and n-1, 
                                        where `n = int(np.sqrt(len(MS)))` is the total number of nodes.
        S (set of tuples/list of int): (sub)set of simple cycles. Each simple cycle is a sequence (tuple/list) of nodes (int).
        Ms (1D array, optional): the sum of all (non-normalized) weight matrices over the simplex S. 
                                 Optional, can be passed to avoid redundant computations. 
                                 Defaults to None
        Ms_is_c0_irred (bool, optional): True if Ms is c0-irreducible. 
                                         Optional, can be passed to avoid redundant computations. 
                                         Defaults to None
        c0_support_of_Ms (bool, optional): c0-support of Ms. 
                                           Optional, can be passed to avoid redundant computations. 
                                           Defaults to None
    Returns:
        bool: True if second claim is satisfied.
    """
    if Ms is None:
        # Compute Ms
        Ms = np.sum([W(c) for c in S],axis=0,dtype=int)
    if Ms_is_c0_irred is None or c0_support_of_Ms is None:
        # Compute the c0-support of Ms if not given as argument
        Ms_is_c0_irred, c0_support_of_Ms = is_c0_irreducible(c0=c0,M=Ms,return_support=True)

    if not Ms_is_c0_irred:
        # Ms must be c0-irreducible
        return False
    else:
        for c in S:
            Mt = Ms - np.lcm(len(c),len(c0))/len(c)*W(c)
            Mt_is_c0_irred,c0_support_of_Mt = is_c0_irreducible(c0=c0,M=Mt,return_support=True)
            if Mt_is_c0_irred and A_has_no_negative_entries_outside_B(A=Mt,B=W(c0)) and c0_support_of_Ms==c0_support_of_Mt:
                return True
        return False

def satisfy_third_claim(c0,S,Ms=None,Ms_is_c0_irred=None,c0_support_of_Ms=None):
    """Return True if the third claim in the appendix ``Polytopes associated to graph 
    -> computing the vertices of p_N(G) -> making the computation more efficient`` is satisfied.

    Args:
        c0 (tuple or list of integers): sequence of node. A node is represented by an integer between 0 and n-1, 
                                        where `n = int(np.sqrt(len(MS)))` is the total number of nodes.
        S (set of tuples/list of int): (sub)set of simple cycles. Each simple cycle is a sequence (tuple/list) of nodes (int).
        Ms (1D array, optional): the sum of all (non-normalized) weight matrices over the simplex S. 
                                 Optional, can be passed to avoid redundant computations. 
                                 Defaults to None
        Ms_is_c0_irred (bool, optional): True if Ms is c0-irreducible. 
                                         Optional, can be passed to avoid redundant computations. 
                                         Defaults to None
        c0_support_of_Ms (bool, optional): c0-support of Ms. 
                                           Optional, can be passed to avoid redundant computations. 
                                           Defaults to None
    Returns:
        bool: True if third claim is satisfied.
    """
    if Ms is None:
        # Compute Ms
        Ms = np.sum([W(c) for c in S],axis=0,dtype=int)
    if Ms_is_c0_irred is None or c0_support_of_Ms is None:
        # Compute the c0-support of Ms if not given as argument
        Ms_is_c0_irred, c0_support_of_Ms = is_c0_irreducible(c0=c0,M=Ms,return_support=True)

    if not Ms_is_c0_irred:
        # Ms must be c0-irreducible
        return False
    else:
        for c1,c2 in itertools.combinations(S,2):
            if len(c1) == len(c2):
                Ms1 = Ms + w(c1) - w(c2)
                Ms2 = Ms + w(c2) - w(c1)
                Ms1_is_c0_irred,c0_support_of_Ms1 = is_c0_irreducible(c0=c0,M=Ms1,return_support=True)
                Ms2_is_c0_irred,c0_support_of_Ms2 = is_c0_irreducible(c0=c0,M=Ms2,return_support=True)
                if Ms1_is_c0_irred and A_has_no_negative_entries_outside_B(A=Ms1,B=W(c0)) and c0_support_of_Ms1==c0_support_of_Ms and \
                    Ms2_is_c0_irred and A_has_no_negative_entries_outside_B(A=Ms2,B=W(c0)) and c0_support_of_Ms2==c0_support_of_Ms:
                    return True
        return False

# ===== Set of faces that satisfy the claims ====== #
# A simplicial complex is a list of set. The list is ordered with respect to length
# Each set identifies a simplex by a collection of vertices

def apply_claims(c0,delta,verbose=False):
    """Apply the first, second and third claims given in ``Polytopes associated to graph 
    -> computing the vertices of p_N(G) -> making the computation more efficient``.
    These claims are used to reduce the list of simplices containing a potential vertex.

    Note before running the method, one must set the graph, denoted G, in graph_utils 
    using the command `set_graph(G)`.

    Args:
        c0 (tuple or list of integers): sequence of node. A node is represented by an integer between 0 and n-1, 
                                        where `n = int(np.sqrt(len(MS)))` is the total number of nodes.
        delta (list of sets): list of simplices. A simplex is represented by a set of simple cycles.
        verbose (bool, optional): if True prints the progression in the terminal. Defaults to False.

    Returns:
        list of sets: smaller list of simplices. Simplices have been discarded according to claims 1,2 and 3.
    """
    G = get_graph()
    delta = list(delta)
    delta.sort(key=len)
    i = 0
    while i < len(delta):
        if verbose:
            print(f'Progression: {i}/{len(delta)}')

        S = delta[i]
        if len(S) == 0:
            # Keep the empty simplex in the delta set
            i += 1
        else:
            Ms = np.sum([W(c) for c in S],axis=0,dtype=int)
            Ms_is_c0_irred, c0_support_of_Ms = is_c0_irreducible(c0=c0,M=Ms,return_support=True)
            if not Ms_is_c0_irred:
                # 1st claim is not satisfied -> discard S
                delta.remove(S)
            elif satisfy_second_claim(Ms=Ms,c0=c0,S=S,Ms_is_c0_irred=True,c0_support_of_Ms=c0_support_of_Ms) or satisfy_third_claim(Ms=Ms,c0=c0,S=S,Ms_is_c0_irred=True,c0_support_of_Ms=c0_support_of_Ms):
                # 2nd or 3rd claim is satisfied -> discard S
                delta.remove(S)
                if c0_support_of_Ms == set(range(G.number_of_nodes())):
                    # Ms is full support -> discard all the faces containing S
                    delta = [s for s in delta if not S <= s] #
            else:
                i += 1
    return delta

# ========================= Get the list of error terms =============================== #

def get_simple_cycle_associated_to_ray(ray,c0):
    """Finds the simple cycle assocaited to the ray, i.e. returns the simple cycle c 
    such that w(c)-w(c0) = rays (up to a multiplictive factor).

    Note before running the method, one must set the graph, denoted G, in graph_utils 
    using the command `set_graph(G)`.

    Args:
        ray (1D numpy array): a ray is a vector of length n^2 proportional to w(c)-w(c0).
                              Note a ray is "normalized" such that it contains the smallest 
                              possible integer values.
        c0 (tuple or list of integers): sequence of node. A node is represented by an integer between 0 and n-1, 
                                        where `n = int(np.sqrt(len(MS)))` is the total number of nodes.

    Raises:
        Exception: if no simple cycle has been found

    Returns:
        (tuple or list of nodes of G): Returns the simple cycle c 
                                       such that w(c)-w(c0) = rays (up to a multiplictive factor)
    """
    w0 = w(c0)
    for c in get_simple_cycles():
        tmp = (w(c)-w0)*np.lcm(len(c),len(c0))
        if (np.asarray(ray) == tmp).all():
            return c
    raise Exception(f'No vertex associated to ray {ray}')

def find_lattice_points_in_S(c0,S,r):
    """Finds the lattice points in the simplex S that are at the border of the polyhedron C_{c0,r}.

    Args:
        c0 (tuple or list of integers): sequence of node. A node is represented by an integer between 0 and n-1, 
                                        where `n = int(np.sqrt(len(MS)))` is the total number of nodes.
        S (set of tuples/list of int): (sub)set of simple cycles. Each simple cycle is a sequence (tuple/list) of nodes (int).
        r (int): positive integer smaller than the length of the simple cycle c0.
                 Namely, r := len(c0)%N


    Returns:
        set: a set of lattice points in the simplex S that are at the border of the polyhedron C_{c0,r}.
    """
    out = set()

    # Construct the lambda coefficients...
    lambda_range = [range(1,int(np.lcm(len(c0),len(c))/len(c) + 2)) for c in S] # 1,2,...,lcm(l0,li)/li + 1
    for lam_vec in itertools.product(*tuple(lambda_range)):
        lam = {tuple(c):lam_vec[i] for i,c in enumerate(S)} 

        # We want only points in the cone: lam[c0]len(c0) + sum_c lam[c]len(c) = r
        tmp = np.sum([lam[c]*len(c) for c in S])
        if tmp%len(c0) == r:
            lam[c0] = -(tmp // len(c0)) # Since we want lattice points, lam[c0] is the integer satisfying the cone constraint
            eps = lam[c0]*W(c0) + np.sum([lam[c]*W(c) for c in S],axis=0) # a lattice point in S (with translation in the direction W(c0))
            if is_c0_irreducible(c0=c0,M=eps):
                # eps is in E since it is a connected lattice point in the cone.
                # Now check if eps is also in E'.
                is_in_E_prime = True
                for c in S:
                    # Define eps'
                    eps_ = eps + np.lcm(len(c0),len(c))/len(c0)*W(c0) - np.lcm(len(c0),len(c))/len(c)*W(c)

                    # Check if eps' is in E
                    if A_has_no_negative_entries_outside_B(A=eps_,B=W(c0)) and is_c0_irreducible(c0=c0,M=eps_):
                        # eps' is in E => eps is not in E'
                        is_in_E_prime = False
                        break
                
                # Add eps to out
                if is_in_E_prime:
                    out.update({tuple(eps)})
    return out

def error_polyhedron(graph,c0,r):
    """Given a graph, a simple cycle and a integer r.
    It computes the polyhedron whose vertices are error terms.

    The vertices of this polyhedron are the "error terms" e such that
    a*W(c) + e are potential vertices of P_N.

    Args:
        graph (networkx.DiGraph): a graph, denoted G.
        c0 (tuple or list of int): sequence of nodes (int) representing a simple cycle of the graph G.
        r (int): positive integer smaller than the length of the simple cycle c0.
                 Namely, r := len(c0)%N

    Returns:
        sage.Polyhedron: polyhedron whose vertices are the error terms
    """
    # Set the graph into an internal representation graph_utils.MyGraph
    set_graph(graph)
    simple_cycles = get_simple_cycles()
    c0 = tuple(c0)
    repeat = 0
    while c0 not in simple_cycles and repeat < len(c0):
        c0 = c0[-1:] + c0[:-1]
        repeat += 1

    # Compute the cone
    vertices = [r*w(c0)]
    rays = [w(c)-w(c0) for c in simple_cycles]
    polyhedron = sage.Polyhedron(vertices=vertices,rays=rays,base_ring=sage.QQ,backend='normaliz') #construct cone
    
    # The polyhedron method from sage re-orders the list of vertices and rays.
    # Therefore, we must re-construct the list of rays.
    C = [] # list of simple cycles in the order compatible with the internal representation of sage.Polyhedron
    for ray in polyhedron.rays():
        C.append(get_simple_cycle_associated_to_ray(ray=ray, c0=c0))
    
    # Compute the triangulation using Normaliz 
    triangulation = polyhedron.triangulate(engine='normaliz') # https://doc.sagemath.org/html/en/reference/discrete_geometry/sage/geometry/polyhedron/base7.html#sage.geometry.polyhedron.base7.Polyhedron_base7.triangulate
    simplicial_complex = triangulation.simplicial_complex() # Note this returns the indices corresponding to the vertices in C, to get the vertices of the i-th simplex: S = [C[j] for j in simplicial_complex[i]]
    delta = [set(C[k] for k in simplex) for simplex in simplicial_complex.face_iterator()]
    
    # Apply claims
    delta = apply_claims(c0=c0,delta=delta)

    # For all the remaining simplices, find the points that are in E'. 
    # Start with empty set
    E_prime = set()
    for S in delta:
        E_prime.update(find_lattice_points_in_S(c0=c0,S=S,r=r))

    # Compute the cone with rays = [w(c)-w(c0) for c in graph.simple_cycles] and vertices=E_prime
    polyhedron_ = sage.Polyhedron(vertices=E_prime,rays=rays,base_ring=sage.QQ,backend='normaliz') #construct cone

    return polyhedron_

def error_terms(graph,c0,r):
    """Given a graph, a simple cycle and a integer r.
    It computes the list of error terms ($\tilde{err}_{\Gamma}(c0,r)$ in the paper).

    The points a*W(c) + e, where e are the error terms,
    are potential vertices of P_N.

    Args:
        graph (networkx.DiGraph): a graph, denoted G.
        c0 (tuple or list of int): sequence of nodes (int) representing a simple cycle of the graph G.
        r (int): positive integer smaller than the length of the simple cycle c0.
                 Namely, r := len(c0)%N

     Returns:
        2D numpy array: each line of the array correspond to an error term.
                        An error term is a flatten matrix of dimension nxn, 
                        where n is the number of nodes in the graph.
                        Error terms contain only integers and the sum of their 
                        elements is zero.

    """
    polyhedron_ = error_polyhedron(graph,c0,r)

    # The error terms correspond to the vertices. Note the error terms are all be integers by construction
    err_ = np.array([v for v in polyhedron_.vertices()],dtype=int)

    return err_

def list_of_error_terms(graph):
    """computes all the error terms for a given graph

    Args:
        graph (networkx.DiGraph): a graph, denoted G.

    Return:
        dict of dict: such that out[c][r] gives the list of error terms for the simple cycle c and integer r.
    """
    G = graph

    pbar = tqdm(total=np.sum([len(c) for c in graph_utils.get_sorted_simple_cycles(G)]),desc='Computing the list of all error terms. Processed simple cycles 0.')
    err_terms = dict() # compute list of all error terms
    for i,c in enumerate(graph_utils.get_sorted_simple_cycles(G)):
        tmp = dict()
        for r in range(len(c)):
            pbar.set_description(f'Computing the list of all error terms. Processed simple cycles {i}. c = {c}, r = {r}')
            tmp[r] = error_terms(graph=G,c0=c,r=r)
            pbar.update(1)
        err_terms[c] = tmp
    pbar.close()

    return err_terms

# ==================== User friendly function =================== #

def number_of_error_terms(graph):
    """Given a graph, computes the number of error terms.
    Returns the number as a dictionary

    Args:
        graph (networkx.DiGraph): a graph, denoted G.
    
    Return:
        dict of list: the output is a dictionary such that out[l0][r] is equal to the number of error terms
                      for simple cycles of length l0 and non-negative integer r smaller than l0.
    """
    out = dict()
    for c in nx.simple_cycles(graph):
        l0 = len(c)
        out[l0] = out.get(l0,{'number of simple cycles':0,'number of error terms':[]}) # creat empty dictionary if does not exist already
        out[l0]['number of simple cycles'] = out[l0]['number of simple cycles'] + 1
        if len(out[l0]['number of error terms']) == 0:
            for r in range(l0):
                (out[l0]['number of error terms']).append(len(error_terms(graph=graph,c0=c,r=r)))
    return out




def main1():
    """Returns the list of error terms for a given simple cycle and integer r.
    """
    import ast

    # Set the graph 
    G_str = input('Enter graph (default is the complete four node graph: K(4)):')
    if G_str == '' or G_str == 'K(4)':
        graph = graph_utils.complete_graph(Nstrat=4)
    else:
        raise Exception('Graph not implemented')
    
    # Set the simple cycle
    c0_str = input('Enter the simple_cycle:')
    c0 = ast.literal_eval(c0_str)
    if not graph_utils.is_simple_cycle(c=c0,graph=graph):
        raise Exception(f'Unknown simple cycle: {c0}')

    # Set the integer r
    r_str = input('Enter the integer r:')
    r = int(r_str)
    if r < 0 or r >= len(c0):
        raise Exception(f'r must be between [0 , {len(c0)-1}]')

    print(f'The list of error terms for c0={c0} and r={r}')
    err_terms = error_terms(graph=graph,c0=c0,r=r)
    for err in err_terms:
        print(err.tolist())

def main2():
    # Set the graph 
    G_str = input('Enter graph (default is the complete four node graph: K(4)):')
    if G_str == '' or G_str == 'K(4)':
        graph = graph_utils.complete_graph(Nstrat=4)
    else:
        raise Exception('Graph not implemented')

    n_err_terms = number_of_error_terms(graph=graph)

    print('The number of error terms for simple cycle of length l0 and non-negative integer r:')
    for l0 in n_err_terms:
        for r in range(l0):
            print(f'l0 = {l0} , r = {r} : {n_err_terms[l0][r]}')



if __name__ == "__main__":
    main1() # list of the error terms for a single simple cycle and integer 
    main2() # number of error terms for all simple cycles and integers