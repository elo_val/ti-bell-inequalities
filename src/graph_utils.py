import numpy as np
from fractions import Fraction
import itertools
import networkx as nx
import matplotlib.pyplot as plt

try:
    from common_utils import tostring, frac_array
except:
    from .common_utils import tostring, frac_array

# Convention: nodes of the graph are labeled by integers from 0 to n-1, where n is the number of nodes.


def draw_graph(graph,node_label=False,edge_label=False,title=None):
    """Example:
    G = debruijn_graph(Nstrat=4,R=1)
    draw_graph(G,node_label='strat',edge_label='strat')"""

    G = graph

    # Choose a layout
    pos = nx.circular_layout(G)

    # Draw the nodes
    nx.draw_networkx_nodes(G=G,
                           pos=pos,
                           alpha=0.5 # transparency of the nodes
                           )
    if node_label:
        if node_label == True:
            # if no label is specified, print the node index by default
            labels = {i:i for i in G.nodes}
        else:
            labels = nx.get_node_attributes(G=G,name=node_label)
        nx.draw_networkx_labels(G=G, 
                                pos=pos, 
                                labels=labels,
                                bbox={"alpha": 0}, # transparent background of the text-box
                                font_size=10
                                )

    # Draw the edges
    connectionstyle = "arc3,rad=0.1"
    nx.draw_networkx_edges(G=G, 
                           pos=pos,
                           connectionstyle=connectionstyle, # shape of the edge
                           alpha=0.5 # transparency of the edges
                           )
    if edge_label:
        if edge_label == True:
            # if no label is specified, print the edges by default
            edge_labels = {tuple(i):tuple(i) for i in G.edges}
        else:
            edge_labels = nx.get_edge_attributes(G=G,name=edge_label)
        nx.draw_networkx_edge_labels(G=G, 
                                     pos=pos, 
                                     edge_labels=edge_labels,
                                     label_pos=0.3, # position is 30% of the edge
                                     connectionstyle=connectionstyle, # shape of the edge
                                     font_color="black",
                                     bbox={"alpha": 0} # transparent background of the text-box
                                     )
        
    if title is not None:
        plt.title(title)
    plt.show()

# def matrix_to_graph(F,label='dummy-label',neutral_element=0):
def matrix_to_graph(M,neutral_element,label='dummy-label'):
    n,m = np.shape(M)
    G = nx.DiGraph()
    for i in range(n):
        G.add_node(i)
        for j in range(m):
            if M[i,j] != neutral_element:
                tmp = {label:M[i,j]}
                G.add_edge(i, j, **tmp)
    return G # networkx.DiGraph()

def graph_to_matrix(G,label=None,neutral_element=0):
    nodes = list(G.nodes)
    n = len(nodes)
    out = np.full((n,n),neutral_element,dtype=object)
    for i, j, w in G.edges(data=True):
        if label is not None:
            out[i,j] = w[label]
        else:
            out[i,j] = 1
    return out

def debruijn_graph(Nstrat,R):
    """Constructs a De Bruijn graph with nodes as integer numbers 0,1,2,...,Nstrat^R-1.
    The graph has two attributes: the number of strategies and the interaction range.
    The graph attributes can be accessed by `G.graph['interaction range'] and G.graph['number of strategies']
    Each node also has a label which gives the strategies associated to that node.
    The label of the i-th node can be accessed by `G.nodes[i]['label'].
    Each edge also has a label which gives the stategies associated to that eadge.
    The label of the edge (i,j) can be accesses by `G.edges[i,j]['label']`.

    Args:
        Nstrat (int): number of strategies in the Bell scenario
        R (int): interaction range

    Returns:
        nx.DiGraph: a De Bruijn graph with nodes represented by integer numbers 0,...,Nstrat^R-1.
                    The nodes' label are tuples of R strategies, e.g. if (Nstrat,R)=(4,2), label of node 4 is (1,0) and of node 2 is (0,2)
                    The edges' label are tuples of R+1 strategies, e.g. if (Nstrat,R)=(4,2), label of the edge (4,2) node is (1,0,2)
    """
    attributes = {'number of strategies':Nstrat,'interaction range':R}
    G = nx.DiGraph(**attributes)
    for i,node_label in enumerate(itertools.product(range(Nstrat),repeat=R)):
        G.add_node(i,strat=node_label)
    
    for i,j in itertools.product(G.nodes,repeat=2):
        s = G.nodes[i]['strat']
        t = G.nodes[j]['strat']
        if s[1:] == t[:-1]:
            G.add_edge(i,j,strat=s+(t[-1],))

    return G

def complete_graph(Nstrat):
    return debruijn_graph(Nstrat=Nstrat,R=1)

def get_sorted_simple_cycles(graph):
    return tuple(sorted([tuple(c) for c in nx.simple_cycles(graph)], key=lambda x:(len(x),x)))

# def get_strategy_vector(graph,node_sequence):
#     G = graph
#     return tuple(G.nodes[i]['strat'][0] for i in node_sequence)

def is_closed_path(closed_path,graph):
    G = graph
    s = closed_path
    N = len(s)
    for i in range(N):
        if (s[i],s[(i+1)%N]) not in G.edges:
            return False
    return True

def is_simple_cycle(c,graph):
    N = len(c)
    visited_nodes = set()
    for i in range(N):
        if (c[i],c[(i+1)%N]) not in graph.edges or c[i] in visited_nodes:
            return False
        visited_nodes.update({c[i]})
    return True

def get_weigth_matrix_associated_to_edge(graph,edge):
    edge_index = tuple(edge)
    edge = graph.edges[edge_index]
    weight_matrix = edge.get('weight matrix')
    if weight_matrix is not None:
        return frac_array(weight_matrix)
    else:
        n =  graph.number_of_nodes()
        weight_matrix = np.zeros((n,n),dtype=Fraction)
        weight_matrix[edge_index] = 1
        weight_matrix = np.reshape(weight_matrix,n**2)
        nx.set_edge_attributes(graph, {edge_index: {"weight matrix": tuple(weight_matrix)}})
        return weight_matrix

def compute_weight_matrix_of_closed_path(graph,c,normalized):
    n = graph.number_of_nodes()
    out = np.zeros((n,n),dtype=Fraction)
    for i in range(len(c)):
        out[c[i],c[(i+1)%len(c)]] += 1
        #edge = (c[i],c[(i+1)%len(c)])
        #out += get_weigth_matrix_associated_to_edge(graph=graph,edge=edge)
    out = np.reshape(out,n**2)
    if normalized:
        return Fraction(1,len(c))*out 
    else:
        return out

if __name__ == "__main__":
    G = debruijn_graph(4,1)
    print(G.graph)

    nx.set_edge_attributes(G, {(1, 2): {"correlator vector": (Fraction(1,2),Fraction(1,1))}})
    edges = G.edges[1,2]
    print(edges)

    print(set(list(G.nodes)))
    print(G.edges)
    print([tuple(c) for i,c in enumerate(nx.simple_cycles(G)) if i <= 20])
    
    print(is_closed_path((0,1,1),G))
