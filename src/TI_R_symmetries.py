import numpy as np
import itertools
from fractions import Fraction
from tqdm import tqdm

try:
    from common_utils import tostring,frac_array
except:
    from .common_utils import tostring,frac_array

# An TI-R inequality is given by the vector (alpha,beta), such that alpha*x >= beta
# For example, when m=R=2, we have (a_0,a_1 , a[r=1]_00,a[r=1]_01,a[r=1]_10,a[r=1]_11 , a[r=2]_00,a[r=2]_01,a[r=2]_10,a[r=2]_11 , beta)

def generate_all_input_flip(R,m,alpha):
    out = set()
    for perm in itertools.permutations(range(m)):
        perm = list(perm)
        tmp = alpha

        # 1 body 
        a = np.array(tmp[0:m])
        tmp = tmp[m:]
        new_alpha = tuple(a[perm])

        # 2 body
        for _ in range(R):
            a = np.array(tmp[0:m**2]).reshape((m,m))
            tmp = tmp[m**2:]
            a = a[perm,:] # permute x input
            a = a[:,perm] # permute y input
            
            new_alpha = new_alpha + tuple(a.flatten())
        out.update({new_alpha})
    return out

def generate_all_output_flip(R,m,alpha):
    out = set()
    for flip in itertools.product([1,-1],repeat=m): 
        # flip[x] = -1 , if we flip outcome when input is x
        #            1 , otherwise
        tmp = alpha

        # 1 body 
        a = np.array(tmp[0:m])
        tmp = tmp[m:]
        new_alpha = tuple(np.array(flip)*a)

        # 2 body 
        for _ in range(R):
            a = np.array(tmp[0:m**2]).reshape((m,m))
            tmp = tmp[m**2:]
            a = np.outer(flip,flip)*a # alpha_xy -> flip[x]*flip[y] alpha_yx
            new_alpha = new_alpha + tuple(a.flatten())
        out.update({new_alpha})
    return out

def generate_all_parties_flip(R,m,alpha):
    out = {tuple(alpha)}

    tmp = alpha

    # 1 body 
    new_alpha = tuple(tmp[:m])
    tmp = tmp[m:]

    # 2 body 
    for _ in range(R):
        a = np.array(tmp[0:m**2]).reshape((m,m))
        tmp = tmp[m**2:]
        a = np.transpose(a) # alpha_xy -> alpha_yx (party flip, the lft-neighbor becomes the right-neighbor)
        new_alpha = new_alpha + tuple(a.flatten())
    out.update({new_alpha})
    return out

 # ============== Classify TI-R vectors (works for both inequalities and vertices) ============ #

def generate_all_equivalent_TIR_vectors(R,m,vector):
    if len(vector) != m+R*m**2:
        raise Exception('The TI-R vector as the wrong length')
    vector = tuple(Fraction(v) for v in vector)
    out = set()
    for a in generate_all_input_flip(R=R,m=m,alpha=vector):
        for b in generate_all_output_flip(R=R,m=m,alpha=a):
            for c in generate_all_parties_flip(R=R,m=m,alpha=b):
                out.update({c})
    return out

def classify_TIR_vectors(R,m,vectors,return_full_classes=False):
    vectors = np.asarray(vectors,dtype=Fraction)
    classes = []
    for vec in vectors:
        new_class = True
        for clas in classes:
            if tuple(vec) in clas:
                new_class = False
                break
        if new_class:
            clas = generate_all_equivalent_TIR_vectors(R=R,m=m,vector=vec)
            classes.append(clas)

    class_reps = []
    for clas in classes:
        class_reps.append(sorted(clas)[0]) # the representative is the 1st element of the sorted list

    if return_full_classes:
        return np.asarray(class_reps,dtype=Fraction),classes
    else:
        return np.asarray(class_reps,dtype=Fraction)


# ================= Classify inequalities ==================== #

def normalize_inequality(ineq):
    """We want integers only"""
    ineq = np.asarray(ineq,dtype=Fraction)
    factor = int(np.lcm.reduce([Fraction(a).denominator for a in ineq])) # if we want only integer coeff
    out = factor*ineq
    return out

def generate_all_equivalent_ineq(R,m,ineq):
    alpha = tuple(ineq[:-1])
    beta = ineq[-1]
    out = generate_all_equivalent_TIR_vectors(R,m,vector=alpha)
    return {alpha + (beta,) for alpha in out} # append beta to alpha

def get_ineq_rep(R,m,ineq):
    ineq = normalize_inequality(ineq=ineq)
    tmp = generate_all_equivalent_ineq(R=R,m=m,ineq=ineq)
    return np.array(sorted(tmp)[0],dtype=Fraction)

def classify_inequalities(R,m,inequalities,return_full_classes=False,verbose=False):
    """classify q list of TIR inequalities according to their symmetries"""
    classes = []
    class_reps = []
    pbar = tqdm(inequalities,desc=f'Classifying inequality 0/{len(inequalities)}, number of classes {len(classes)}',disable=not verbose)
    for i,ineq in enumerate(pbar):
        ineq = normalize_inequality(ineq=ineq)
        new_class = True
        for clas in classes:
            if tuple(ineq) in clas:
                new_class = False
                break
        if new_class:
            tmp = generate_all_equivalent_ineq(R=R,m=m,ineq=ineq)
            classes.append(tmp)
            class_reps.append(sorted(tmp)[0]) # the representative is the 1st element of the sorted list
        pbar.set_description(f'Classifying inequality {i}/{len(inequalities)}, number of classes {len(classes)}')

    if return_full_classes:
        return np.asarray(class_reps),classes
    else:
        return np.asarray(class_reps)


def random_tests():
    R = 1
    m = 2
    alpha = (1,2,3,4,5,6)
    beta = Fraction(1,20)
    ineq = frac_array(alpha + (beta,))

    # input permutations
    N_input_perm = np.prod([i for i in range(1,m+1)])
    input_perm = generate_all_input_flip(R=R,m=m,alpha=alpha)
    print(f'Number of input permutations (for m={m}):',N_input_perm,len(input_perm))

    # outcome permutations
    N_outcome_perm = 2**m
    outcome_perm = generate_all_output_flip(R=R,m=m,alpha=alpha)
    print(f'Number of outcome permutations (for m={m}):',N_outcome_perm,len(outcome_perm))

    # parties permutations
    N_parties_perm = 2
    parties_perm = generate_all_parties_flip(R=R,m=m,alpha=alpha)
    print(f'Number of party permutations:',N_parties_perm,len(parties_perm))

    # inequality permutation
    N_ineq_perm = N_input_perm*N_outcome_perm*N_parties_perm
    ineq_perm = generate_all_equivalent_ineq(R=R,m=m,ineq=ineq)
    print(f'Total number of inequality permutations:',N_ineq_perm,len(ineq_perm))

    # Normalize ineq
    ineq2 = normalize_inequality(ineq=ineq)
    print(f'The normalized inequality is {tostring(ineq2)}')

    # Class representative
    clas_rep = get_ineq_rep(ineq=ineq2,R=R,m=m)
    print(f'The representative of the class is {tostring(clas_rep)}')


def main():
    m = 2
    R = 1

    # TINN inequality classes (inequalities from the paper)
    i1 = frac_array([2,0,1,0,0,0,-1])
    i2 = frac_array([1, 1, 0, 0, 1, 0, -1])
    i3 = frac_array([ 2, 0, 1,-1, 1,-1, -2])
    i4 = frac_array([ 0, 0, 2,-1, 1, 0,-2])
    i5 = frac_array([ 0, 0, 1, 0, 2,-1,-2])
    i6 = frac_array([0,0,-2,-1,1,0,-2])

    # Generate all TINN inequalities from the class representative
    generated_TINN_ineqs = set()
    generated_TINN_ineqs.update(generate_all_equivalent_ineq(R=R,m=m,ineq=i1))
    generated_TINN_ineqs.update(generate_all_equivalent_ineq(R=R,m=m,ineq=i2))
    generated_TINN_ineqs.update(generate_all_equivalent_ineq(R=R,m=m,ineq=i3))
    generated_TINN_ineqs.update(generate_all_equivalent_ineq(R=R,m=m,ineq=i4))
    generated_TINN_ineqs.update(generate_all_equivalent_ineq(R=R,m=m,ineq=i5))
    generated_TINN_ineqs.update(generate_all_equivalent_ineq(R=R,m=m,ineq=i6))

    # All TINN inequalities
    TINN_ineqs = [[0, 2, 0, 0, 0, 1, -1], 
                [0, 0, 0, 1, -1, 2, -2], 
                [0, 0, 0, -1, 1, 2, -2], 
                [0, 2, -1, -1, 1, 1, -2], 
                [0, 0, -1, 0, 2, 1, -2], 
                [0, 0, -1, 2, 0, 1, -2], 
                [0, 2, -1, 1, -1, 1, -2], 
                [0, 0, 2, -1, 1, 0, -2], 
                [2, 0, 1, 0, 0, 0, -1], 
                [2, 0, 1, -1, 1, -1, -2], 
                [2, 0, 1, 1, -1, -1, -2], 
                [0, 0, 2, 1, -1, 0, -2], 
                [0, 0, 1, 2, 0, -1, -2], 
                [1, 1, 0, 1, 0, 0, -1], 
                [0, 0, 1, 0, 2, -1, -2], 
                [1, 1, 0, 0, 1, 0, -1], 
                [-2, 0, 1, -1, 1, -1, -2], 
                [-2, 0, 1, 1, -1, -1, -2], 
                [0, 0, 0, -1, 1, -2, -2], 
                [0, 0, 0, 1, -1, -2, -2], 
                [1, -1, 0, 0, -1, 0, -1], 
                [0, 0, 1, 0, -2, -1, -2], 
                [1, -1, 0, -1, 0, 0, -1], 
                [0, 0, 1, -2, 0, -1, -2], 
                [0, -2, -1, 1, -1, 1, -2], 
                [0, 0, -1, 0, -2, 1, -2], 
                [0, -2, -1, -1, 1, 1, -2], 
                [0, 0, -1, -2, 0, 1, -2], 
                [0, 0, -2, 1, -1, 0, -2], 
                [0, 0, -2, -1, 1, 0, -2], 
                [0, -2, 0, 0, 0, 1, -1], 
                [-1, 1, 0, 0, -1, 0, -1], 
                [-1, -1, 0, 1, 0, 0, -1], 
                [-1, -1, 0, 0, 1, 0, -1], 
                [-1, 1, 0, -1, 0, 0, -1], 
                [-2, 0, 1, 0, 0, 0, -1]]
    
    TINN_ineqs = {tuple(ineq) for ineq in TINN_ineqs}

    print(f'Size of the two sets {len(generated_TINN_ineqs)} and {len(TINN_ineqs)}')
    print(f'Difference between the two sets: {generated_TINN_ineqs-TINN_ineqs}')


if __name__ == "__main__":
    random_tests()
    print('\n')
    main()
