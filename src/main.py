import numpy as np
import networkx as nx
from fractions import Fraction
import pickle
from tqdm import tqdm

try:
    from common_utils import tostring, frac_array, array_to_tuple
    import minkowsky_weyl
    import TI_R_projection
    import graph_polytopes
    import graph_utils
    import tropical_algebra
    import TI_R_symmetries
    import error_terms
except:
    from .common_utils import tostring, frac_array, array_to_tuple
    from . import minkowsky_weyl
    from . import TI_R_projection
    from . import graph_polytopes
    from . import graph_utils
    from . import tropical_algebra
    from . import TI_R_symmetries
    from . import error_terms

def section_II_A_single_party_correlators():
    """The one-body correlator vectors associated to each strategy in the case m=d=2"""
    m = 2 # number of inputs
    d = 2 # number of outcomes
    for s in range(d**m): # there are 4 strategies in the case m=d=2
        print(f'\t<A>_{s} = ',TI_R_projection.one_body_corr_vec(strat=s,m=m,d=d))

def section_II_B_1_eigenvalues():
    """Example of eigenvalues of a tropical matrix"""
    F = np.array([[2,4,-4,-2],[0,2,-2,0],[0,-2,2,0],[-2,-4,4,2]])
    eigval=tropical_algebra.compute_eigval(F)
    print(f'The matrix\n{F}\nhas eigenvalue {eigval}')

    # minimum normalized weight (i.e. weight divided by the number of edges) of a directed cycle in G_F
    G = graph_utils.matrix_to_graph(M=F,neutral_element=np.inf)
    lam = np.inf
    for c in nx.simple_cycles(G):
        tmp = np.sum([F[c[i],c[(i+1) % len(c)]] for i in range(len(c))])/len(c)
        lam = np.minimum(lam,tmp)
    print(f'Minimum normalized weight of a cycle; {lam}')

def section_II_B_2_kleene_plus():
    """Example of Kleeneplus"""
    F = np.array([[2,4,-4,-2],[0,2,-2,0],[0,-2,2,0],[-2,-4,4,2]])
    F_kleeneplus_4 = tropical_algebra.kleeneplus(F=F,k=4)
    print(f'Let the matrix F =\n{F}.\nThen the 4-th Kleene-plus is F^(+4) =\n{F_kleeneplus_4}')

def section_II_B_3_eigenvector():
    """Example of eigenvalues and eigenvectors of a tropical matrix"""
    F = np.array([[2,4,-4,-2],[0,2,-2,0],[0,-2,2,0],[-2,-4,4,2]])
    eig_value=tropical_algebra.compute_eigval(F) 
    eig_vec=tropical_algebra.compute_eigvecs(F,eigval=eig_value)
    print(f'The matrix\n{F}\nhas eigenvalue {eig_value} and eigenvectors\n{eig_vec}')

def section_II_B_4_critical_graph():
    """Example of critical graph"""
    F = np.array([[2,4,-4,-2],[0,2,-2,0],[0,-2,2,0],[-2,-4,4,2]])
    F_crit = tropical_algebra.compute_critical_graph(F)
    print(f'Weight matrix of the critical graph:\n{F_crit}')
    G_crit = graph_utils.matrix_to_graph(M=F_crit,neutral_element=np.inf,label='weight')
    graph_utils.draw_graph(G_crit,node_label=True,edge_label='weight',title=f'Critical graph of the matrix F')

    # Print the simple cycles
    G_crit = graph_utils.matrix_to_graph(M=F_crit,neutral_element=np.inf)
    simple_cycles = {tuple(c) for c in nx.simple_cycles(G_crit)}
    print(f'The simple cycles in the critical graph:\n{simple_cycles}')

def section_II_B_5_tropical_power():
    """Example of tropical power and cyclicity"""
    F = np.array([[2,4,-4,-2],[0,2,-2,0],[0,-2,2,0],[-2,-4,4,2]])
    sigma = tropical_algebra.cyclicity(F)
    sigma,N0 = tropical_algebra.stabilization_numbers(F,sigma=sigma)
    print(f'The stabilization numbers are sigma = {sigma} and N0 = {N0}')

def section_III_C_figure_4():
    """This example corresponds to the figure 4 in the paper"""

    # one-body correlator vectors associated to each strategy in the case m=d=2
    m = 2 # number of inputs
    d = 2 # number of outcomes
    print(f'Correlator vectors associated to nodes:')
    for s in range(d**m): # there are 4 strategies in the case m=d=2
        print(f'\t<A>_{s} = ',TI_R_projection.one_body_corr_vec(strat=s,m=m,d=d))

    # Correlator vector associated to edge
    edge = (1,3)
    corr_vec = TI_R_projection.symmetric_R_corr_vec(strategy_vector=edge,m=2,d=2,R=1)
    print(f'Correlator vector associated to edge {edge}: {tostring(corr_vec)}')

    # Correlator vector associated to the closed path
    N = 5 # number of parties
    m = 2 # number of inputs
    d = 2 # number of outcomes (code supports only d=2 case)
    R = 1 # interaction range
    scenario = (N,m,d)

    # Sum the edges
    edges = [(0,0),(0,1),(1,3),(3,1),(1,0)]
    tmp2 = np.sum([TI_R_projection.symmetric_R_corr_vec(strategy_vector=s,m=2,d=2,R=1) for s in edges],axis=0)/len(edges)
    print('Sum of the edges:',tostring(tmp2))

    # TI-R projection
    s = (0,0,1,3,1) # strategy
    tmp1 = TI_R_projection.TI_R_corr_vec(scenario=scenario,R=R,strategy_vector=s)
    print('TINN projection of the corresponding strategy:',tostring(tmp1))

def section_III_C_example_theorem_1():
    """Compute the TINN local polytope (*,2,2) and (24,2,2)"""
    N = 24 # number of parties
    m = 2 # number of inputs
    d = 2 # number of outcomes (code supports only d=2 case)
    R = 1 # interaction range

    # Associated graph
    graph = graph_utils.complete_graph(Nstrat=d**m)

    # vertices of L_star
    vertices_p_star = graph_polytopes.star_polytope(graph=graph)
    vertices_p_star = np.reshape(vertices_p_star,(-1,d**(R*m),d**(R*m))) # reshape all the vertices as matrices
    points_L_star = np.empty((len(vertices_p_star),m+R*m**2),dtype=Fraction)
    for i,w in enumerate(vertices_p_star):
        points_L_star[i,:] = TI_R_projection.Phi_map(graph=graph,w=w)
    _,vertices_L_star = minkowsky_weyl.rays_and_vertices_polyhedron(points=points_L_star) # local polytope (*,m,d)
    print(f'The TI-{R} local polytope for N divisible enough L^* has {np.shape(vertices_L_star)[0]} vertices of dimension {np.shape(vertices_L_star)[1]}')

    # vertices of L_N
    vertices_P_N = graph_polytopes.graph_polytope_smart(N=N,graph=graph,verbose=True)
    vertices_P_N = np.reshape(vertices_P_N,(-1,d**m,d**m)) # reshape all the vertices as matrices
    points_of_L_N = np.empty((len(vertices_P_N),m+R*m**2),dtype=Fraction)
    for i,W in enumerate(vertices_P_N):
        w = Fraction(1,N)*W
        points_of_L_N[i,:] = TI_R_projection.Phi_map(graph=graph,w=w)
    _,vertices_L_N = minkowsky_weyl.rays_and_vertices_polyhedron(points=points_of_L_N) # local polytope (N,m,d), note we need the normalized we
    print(f'The TI-{R} local polytope L^N has {np.shape(vertices_L_N)[0]} vertices of dimension {np.shape(vertices_L_N)[1]}')

    # Check if the vertices of L^N == L^*
    are_equal = set(tuple(v) for v in vertices_L_star) == set(tuple(v) for v in vertices_L_N)
    print(f'The two polytopes are equal: {are_equal}')

    # Classify the vertices according to symmetries
    vertex_classes = TI_R_symmetries.classify_TIR_vectors(R=R,m=m,vectors=vertices_L_star)
    print(f'There are {len(vertex_classes)} vertex classes:\n{tostring(vertex_classes)}')

    # Compute the inequalities
    eq,ineq = minkowsky_weyl.equalities_and_inequalities_polyhedron(points=vertices_L_star)
    print(f'There are {len(eq)} equalities describing the TI-{R} local polytope L^*'+(len(eq)==0)*', i.e. it is full dimensional.')
    print(f'There are {len(ineq)} inequalities describing the TI-{R} local polytope L^*')

    # Classify the inequalities
    classes = TI_R_symmetries.classify_inequalities(R=R,m=m,inequalities=ineq)
    print(f'There are {len(classes)} inequality classes:\n{tostring(classes)}')

def section_IV_C_debruijn_graph():
    """An example of a De Bruijn graph"""
    Nstrat = 2
    R = 3
    G = graph_utils.debruijn_graph(Nstrat=Nstrat,R=R)
    graph_utils.draw_graph(G,node_label='strat',edge_label='strat',title=f'Example of De Bruijn graph with {Nstrat} strategies and interaction range {R}')


def section_IV_C_example_theorem_2_part_1():
    """Computing the vertices of the TI-2 local polytope (*,2,2)"""
    m = 2 # number of inputs
    d = 2 # number of outcomes (code supports only d=2 case)
    R = 2 # interaction range
    print(f'Construct and analyze the TI-{R} local polytope (*,{m},2)')

    # Associated graph
    graph = graph_utils.debruijn_graph(Nstrat=d**m,R=R)

    # vertices of p_star 
    vertices_p_star = graph_polytopes.star_polytope(graph=graph,verbose=True)
    print(f'The normalized cycle polytope has {np.shape(vertices_p_star)[0]} vertices of dimension {np.shape(vertices_p_star)[1]}.')

    # vertices of L_star
    vertices_p_star = np.reshape(vertices_p_star,(-1,d**(R*m),d**(R*m))) # reshape all the vertices as matrices
    points_of_L_star = []
    for w in tqdm(vertices_p_star,desc='Computing correlator vectors for all weight-matrices'):
        points_of_L_star.append(TI_R_projection.Phi_map(graph=graph,w=w))
    points_of_L_star = np.array(points_of_L_star)
    points_of_L_star = frac_array(np.unique(tostring(points_of_L_star),axis=0)) # remove duplicated elements
    print('Computing V-representation of the convex hull of all correlator vectors:')
    _,vertices_of_L_star = minkowsky_weyl.rays_and_vertices_polyhedron(points=points_of_L_star,verbose=True)
    print(f'The TI-{R} local polytope for N divisible enough L^* has {np.shape(vertices_of_L_star)[0]} vertices of dimension {np.shape(vertices_of_L_star)[1]}')

    # Save the variable using pickle
    with open("vertices_of_TI2.pkl", 'wb') as file:
        pickle.dump(vertices_of_L_star, file) 


def section_IV_C_example_theorem_2_part_2():
    """Classifying the vertices of the TI-2 local polytope (*,2,2)"""
    m = 2 # number of inputs
    d = 2 # number of outcomes (code supports only d=2 case)
    R = 2 # interaction range

    # Load the vertices of the TI-2 local polytope
    with open("vertices_of_TI2.pkl", 'rb') as file:
        vertices_of_L_star = pickle.load(file)

    # Classify the vertices according to symmetries
    vertex_classes = TI_R_symmetries.classify_TIR_vectors(R=R,m=m,vectors=vertices_of_L_star)
    print(f'There are {len(vertex_classes)} vertex classes:\n{tostring(vertex_classes)}')

    # Save the variable using pickle
    with open("vertex_classes_of_TI2.pkl", 'wb') as file:
        pickle.dump(vertex_classes, file) 


def section_IV_C_example_theorem_2_part_3():
    """Computing the inequalities of the TI-2 local polytope (*,2,2)"""
    m = 2 # number of inputs
    d = 2 # number of outcomes (code supports only d=2 case)
    R = 2 # interaction range

    # Load the vertices of the TI-2 local polytope
    with open("vertices_of_TI2.pkl", 'rb') as file:
        vertices_of_L_star = pickle.load(file)

    # Compute the inequalities
    eq,ineq = minkowsky_weyl.equalities_and_inequalities_polyhedron(points=vertices_of_L_star,verbose=True)
    print(f'There are {len(eq)} equalities describing the TI-{R} local polytope L^*'+(len(eq)==0)*', i.e. it is full dimensional.')
    print(f'There are {len(ineq)} equalities describing the TI-{R} local polytope L^*')

    # Save the variable using pickle
    with open("inequalities_of_TI2.pkl", 'wb') as file:
        pickle.dump(ineq, file) 

def section_IV_C_example_theorem_2_part_4():
    """Classifying the inequalities of the TI-2 local polytope (*,2,2) according to their symmetries"""
    m = 2 # number of inputs
    d = 2 # number of outcomes (code supports only d=2 case)
    R = 2 # interaction range

    # Load the vertices of the TI-2 local polytope
    with open("inequalities_of_TI2.pkl", 'rb') as file:
        ineq_of_L_star = pickle.load(file)

    # Classify the inequalities
    ineq_classes = TI_R_symmetries.classify_inequalities(R=R,m=m,inequalities=ineq_of_L_star,verbose=True)
    print(f'There are inequalities {len(ineq_classes)} inequality classes\n{tostring(ineq_classes)}')

    # Save the variable using pickle
    with open("inequality_classes_of_TI2.pkl", 'wb') as file:
        pickle.dump(ineq_classes, file) 

def section_V_vertices_of_closed_path_polytope():
    """Compute the vertices of the closed path polytope for finite N"""
    m = 2 # number of inputs
    d = 2 # number of outcomes (code supports only d=2 case)
    R = 1 # interaction range

    # Associated graph
    G = graph_utils.debruijn_graph(Nstrat=d**m,R=R)

    print(f'Number of vertices of P_N(G) close to w(c), where w(c) is the weigth-matrix associated to the simple cycle c.\nThe number depends on r = N % len(c).')
    for c0 in graph_utils.get_sorted_simple_cycles(G):
        tmp = dict()
        for r in range(len(c0)):
            tmp[f'r={r}'] = len(error_terms.error_terms(graph=G,c0=c0,r=r))
        print(f'Simple cycle c = {c0} : {tmp}')

def section_V_vertices_of_local_polytope():
    """Compute the vertices of the local polytope for finite N"""
    m = 2 # number of inputs
    d = 2 # number of outcomes (code supports only d=2 case)
    R = 1 # interaction range

    # Associated graph
    G = graph_utils.debruijn_graph(Nstrat=d**m,R=R)
    n = G.number_of_nodes()

    err_terms = error_terms.list_of_error_terms(G)

    # vertices of L_N for N = 24,...,47 (12 periodicity)
    for N in range(24,48):
        points = frac_array(np.empty((0,m+R*m**2)))
        for c in graph_utils.get_sorted_simple_cycles(G):
            a = N // len(c)
            r = N%len(c)
            for v in err_terms[c][r]:
                # Ws = a*graph_utils.compute_weight_matrix_of_closed_path(graph=G,c=c,normalized=False) + v
                # corr_vec = TI_R_projection.Phi_map(graph=G,w=np.reshape(Ws,((n,n))))
                wc = graph_utils.compute_weight_matrix_of_closed_path(graph=G,c=c,normalized=True)
                v = frac_array(v)-r*wc
                ws = wc + Fraction(1,N)*v
                corr_vec = TI_R_projection.Phi_map(graph=G,w=np.reshape(ws,((n,n))))
                points = np.concatenate((points,np.reshape(corr_vec,(1,m+R*m**2))),axis=0)
        points = frac_array(np.unique(tostring(points),axis=0)) # remove duplicated elements
        _,vertices = minkowsky_weyl.rays_and_vertices_polyhedron(points=points)
        print(f'The local polytope {(N,m,d)} has {len(vertices)} vertices')


if __name__ == "__main__":
    # Select the example

    # section_II_A_single_party_correlators()
    # print(' ============= ')
    # section_II_B_1_eigenvalues()
    # print(' ============= ')
    # section_II_B_2_kleene_plus()
    # print(' ============= ')
    # section_II_B_3_eigenvector()
    # print(' ============= ')
    # section_II_B_4_critical_graph()
    # print(' ============= ')
    # section_III_C_figure_4()
    # print(' ============= ')
    # section_III_C_example_theorem_1()
    # print(' ============= ')
    # section_IV_C_debruijn_graph()
    # print(' ============= ')
    # section_IV_C_example_theorem_2_part_1()
    # section_IV_C_example_theorem_2_part_2()
    # section_IV_C_example_theorem_2_part_3()
    # section_IV_C_example_theorem_2_part_4()
    # print(' ============= ')
    # section_V_vertices_of_closed_path_polytope()
    # print(' ============= ')
    section_V_vertices_of_local_polytope()
    print(' ============= ')
    print('Finished')