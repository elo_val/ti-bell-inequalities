import numpy as np
from fractions import Fraction
import networkx as nx
import itertools
import graph_utils
from error_terms import error_terms
from tqdm import tqdm

try:
    import minkowsky_weyl
except:
    from . import minkowsky_weyl

# ========= graph polytopes =========== #

def graph_polytope_bruteforce(graph,N):
    """Computes the polytope P_N(G) with the bruteforce method, 
    i.e. enumerates all the closed paths of length N.
    Note the vertices are not normalized, i.e. only integer vectors.

    Args:
        graph (networkx.DiGraph): a graph, denoted G.
        N (int): length of the closed path

    Returns:
        2D numpy array of fractions: vertices of the P_N(G) polytope, where G is the graph.
    """
    # Now, we iterate over all the closed path of length N.
    # Note, there is probably a smarter way to iterate over all the closed path of length N.
    # An evenmore efficient way would be to note that actually we don't care about all the closed path,
    # but only on the different combinations of simple cycles that have a total length of N.
    PN = []
    for s in itertools.product(graph.nodes,repeat=N): 
        if graph_utils.is_closed_path(closed_path=s,graph=graph):
            # Check that s is a closed path in G
            Ws = graph_utils.compute_weight_matrix_of_closed_path(graph=graph,c=s,normalized=False)
            PN.append(Ws)
    _,vertices = minkowsky_weyl.rays_and_vertices_polyhedron(points=PN)
    return vertices

def graph_polytope_smart(graph,N,verbose=False):
    """Computes the polytpe P_N(G) with a method that is independent of N.
    The method is given in the appendix of the paper.
    It is based on the error-terms list.
    Note the vertices are not normalized, i.e. only integer vectors.

    Args:
        graph (networkx.DiGraph): a graph, denoted G.
        N (int): length of the closed path
        verbose (bool, optional): print information in the console if True. Defaults to False.

    Returns:
        2D numpy array of fractions: vertices of the P_N(G) polytope, where G is the graph.
    """

    # Compute the general polytope: PN = conv{N//len(c)*W(c) + v : v in L[c][N%len(c)]}
    PN = []
    pbar = tqdm(nx.simple_cycles(graph), desc='Searching simple cycle...',disable=not verbose)
    for c in pbar:
        c = tuple(c)
        pbar.set_description(f'Computing weight-matrices for all error-terms close to simple-cycle {c}')
        Wc = graph_utils.compute_weight_matrix_of_closed_path(graph=graph,c=c,normalized=False)
        a = N // len(c)
        r = N % len(c)
        err_c_r = error_terms(graph=graph,c0=c,r=r)
        for v in err_c_r:
            PN.append(a*Wc + v)
    pbar.set_description(f'Computing weight-matrices for all error-terms close to simple-cycles. Done')
    _,vertices = minkowsky_weyl.rays_and_vertices_polyhedron(points=PN)
    return vertices

def star_polytope(graph,verbose=False):
    """Computes the polytope p_*(G) by enumerating all the simple cycles.
    Note the vertices are normalized, i.e. use type Fraction

    Args:
        graph (networkx.DiGraph): a graph, denoted G.
        verbose (bool, optional): print information in the console if True. Defaults to False.

    Returns:
        2D numpy array of fractions: vertices of the p_*(G) polytope, where G is the graph.
    """
    P_star = []
    for c in tqdm(nx.simple_cycles(graph),desc='Computing the weight matrices for all simple cycles',disable=not verbose):
        wc = graph_utils.compute_weight_matrix_of_closed_path(graph=graph,c=c,normalized=True)
        P_star.append(wc)
    return np.array(P_star,dtype=Fraction)
    

if __name__ == "__main__":
    graph = graph_utils.complete_graph(Nstrat=4)

    tmp = graph_polytope_bruteforce(graph=graph,N=3)
    print(f'Number of verices of P_3: {len(tmp)}')

    tmp = graph_polytope_smart(graph=graph,N=36,verbose=False)
    print(f'Number of verices of P_36: {len(tmp)}')

    tmp = star_polytope(graph=graph)
    print(f'Number of verices of p_star: {len(tmp)}')

