import numpy as np
from fractions import Fraction
import itertools

def decompose_number(number, base, length):
    """
    Decomposes a given number into digits in the specified base and ensures the resulting list has the desired length.

    Args:
        number (int): The number to decompose.
        base (int): The base in which to decompose the number.
        length (int): The desired length of the resulting list of digits.

    Returns:
        list: A list of digits representing the number in the specified base, zero-padded to match the desired length.
    """
    digits = []
    while number > 0:
        digit = number % base
        digits.insert(0, digit)
        number //= base
    out = (length-len(digits))*[0]+digits
    return tuple(out)  

def frac_array(array,allow_special=[]):
    if allow_special==[]:
        return np.vectorize(lambda x:Fraction(x),otypes=[Fraction])(array)
    else:
        out = np.asarray(array,dtype=Fraction)
        for index in itertools.product(*tuple(range(n) for n in np.shape(out))):
            if out[index] in allow_special:
                pass
            else:
                out[index] = Fraction(out[index])
        return out

def tostr(fraction_array):
    """Convert a numpy array of fraction to a numpy array of strings.
    This makes the reading easier.

    Args:
        fraction_array (numpy array of Fraction): numpy array of fractions

    Returns:
        numpy array of str: numpy array of strings
    """
    return np.vectorize(lambda x:str(x), otypes=[str])(fraction_array)

def tostring(fraction_array):
    """Alias for tostr. Added for retro-compatibility"""
    return tostr(fraction_array)

def rescale_to_int(fraction_array):
    tmp = np.vectorize(lambda x: x.denominator)(fraction_array)
    scale_factor = Fraction(1,int(np.lcm.reduce(tmp.flatten())))
    array_out = (fraction_array/scale_factor).astype(dtype=int)
    return array_out,scale_factor

def array_to_tuple(arr):
    """Recursively convert a NumPy array to a nested tuple.
    This method is usefull if one wants to make sets of numpy arrays."""
    if not isinstance(arr, np.ndarray):
        return arr
    return tuple(array_to_tuple(sub_arr) for sub_arr in arr)

def hashable(fraction_array):
    return array_to_tuple(tostr(fraction_array))

def main():

    # Declare a fraction array
    A = frac_array([[1,0.5,'1/3'],[Fraction(7,5),-3,'-1/6']])
    print(f"Created an array of type: {type(A[0,0])},{type(A[0,0].numerator)},{type(A[0,0].denominator)}")

    # Transform numpy array to frac_array
    B = np.ones((5,2))
    print(f"Created an array of type: {type(B[0,0])}")
    C = frac_array(B)
    print(f"Created an array of type: {type(C[0,0])},{type(C[0,0].numerator)},{type(C[0,0].denominator)}")

    # Allow special characters, to extend algebra
    D = frac_array([np.inf,'x',Fraction(6,5),3,'1/2'],allow_special=[np.inf,'x'])
    print(f'Example of a frac_array with special characters: {D}')

    # print
    print(f'Fancy print: {tostr(D)}')

    # Rescale to int (works only if there are no special characters)
    E=A.reshape(6) # reshape so that it fits on one line
    e,factor = rescale_to_int(E)
    print(f'Original: {tostr(E)}, with type {type(E[0])}\nRescaled {factor}*{e}, the new array has type {type(e[0])} and the factor has type {type(factor)}')

    # To tuple (works with nested array)
    print(f'Tuple version:{array_to_tuple(A)}')

    # hashable
    print(f'Hashable: {hashable(A)}')


if __name__ == "__main__":
    main()