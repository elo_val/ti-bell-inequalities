# Translation-invariant Bell inequalities with tropical algebra and graph polytopes

This repository contains the code supporting the paper [_Characterizing Translation-Invariant Bell Inequalities using Tropical Algebra and Graph Polytopes_](https://arxiv.org/abs/2407.08783)[1].

## Installation
The code is written for Python 3 and tested to work with Python 3.10.12.

To make sure that all requirements of the package are fulfilled, the easiest way to use the code is to create a virtual environment and install the necessary packages independent of the python packages of the operating system

1. Creation of a virtual environment  
You can create the environment in a folder of your choice. 
For the rest of the tutorial, we assume it to be in `~/.pyenv/`
    ```
    cd ~/.pyenv
    python -m venv ti-bell-ineq
    ```
    Assuming you are using bash or zsh, you can activate the environment with `source ~/.pyenv/ti-bell-ineq/bin/activate`.
    Upon activation, you will notice that your prompt changes.
    As long as it is prefixed with `(ti-bell-ineq)` the virtual environment is active.
    The virtual environment can be deactivated with `deactivate`.

2. Preparation of the environment: install the SAGE library  
Follow the instructions [here](https://doc.sagemath.org/html/en/installation/) to install SAGE [2].
Make sure to install SAGE in the virtual environement we created in step 1.

3. Cloning the code  
You can obtain the code by cloning the repo with
    ```
    git clone https://gitlab.com/elo_val/ti-bell-inequalities.git
    ```
4. Preparation of the environment: install other requirements  
For the next step, please navigate into the repo that you just downloaded and activate the empty environment that we created in step 1.
In order to install all required packages for the simulation, execute
    ```
    pip install -r requirements.txt
    ```

## References 

1. Link to the paper related to the code: <https://arxiv.org/abs/2407.08783>
2. Link to SAGE library <https://doc.sagemath.org/html/en/installation/>

## Structure for the Code

All the source files are in the `src/` folder. 
The file `main.py` contains the computations that are done in the paper. 
The other python files define methods and functions that are necessary for the computations.
Summary of all the files:

- `common_utils.py` : contains methods to handle numpy array of fractions.
- `error_terms.py` : contains methods to compute the error-terms necessary to compute the closed path polytope.
- `graph_polytopes.py` : contains methods computing the closed path polytope $p_N(\Gamma)$ and the cycle polytope $p_*(\Gamma)$.
- `graph_utils.py` : contains methods to manipulate graphs (and more specifically De Bruijn graphs).
- `main.py` : contains the computations done in the paper. Each method corresponds to one specific computation in the paper. 
`minkowsky_weyl.py` : contains methods to compute convex-hulls.
- `taminplus.py` : contains the base code for tropical algebra.
- `TI_R_projection.py` : contains methods necessary to express the TI-$R$ projection.
- `TI_R_symmetries.py` : contains methods necessary to classify TI-Bell inequalities according to symmetries.
- `tropical_algebra.py` : contains methods to compute advanced concept in tropical algebra.

## Known issues and future work

- Merge the files `taminplus.py` and `tropical_algebra.py`
- Add code to find quantum violation of TI-2 ineqaulities
